@extends('template.template')

@section('title'.'Order')

@section('content')
<!-- Breadcrumb -->
<div class="page-info-section page-info">
		<div class="container">
			<div class="site-breadcrumb">
				<a href="/">Home</a> /
				<span>Order</span>
			</div>
			<img src="{{asset('img/page-info-art.png')}}" alt="" class="page-info-art">
		</div>
	</div>
<!-- Breadcrumb end -->
<div class="page-area cart-page spad">
		<div class="container">
			<form class="checkout-form">
				<div class="row">
						<div class="col-lg-6">
							<h4 class="checkout-title">Billing Address</h4>
							<div class="row">
								<div class="col-md-12">
								<form action="" method="POST" autocomplete="off">
								@csrf
									<input name="recipient" class="form-control @error('recipient') invalid is-invalid @enderror" type="text" placeholder="Recipient *">
									@error('recipient')
									<span class="invalid"><i>{{$message}}</i></span>
									@enderror
									<input name="address" class="form-control @error('address') invalid is-invalid @enderror" type="text" placeholder="Address *">
									@error('address')
									<span class="invalid"><i>{{$message}}</i></span>
									@enderror
									<input name="telephone" class="form-control @error('telephone') invalid is-invalid @enderror" type="text" placeholder="Phone Number *">
									@error('telephone')
									<span class="invalid"><i>{{$message}}</i></span>
									@enderror
									<select name="shipping" class="form-control @error('telephone') invalid is-invalid @enderror shipping">
										<option>Select Shipping Service *</option>
										<option value="jne">JNE</option>
										<option value="JNT">JNT</option>
										<option value="sicepat">SICEPAT</option>
										<option value="pos">POS INDONESIA</option>
									</select>
									<input name="shippingfee" class="form-control @error('telephone') invalid is-invalid @enderror shippingfee" type="text" placeholder="Shippingfee" readonly>
									<button type="submit" class="site-btn btn-full">Place Order</button>
								</form>
								</div>
							</div>
						</div>
						<div class="aaa col-lg-6">
							<div class="ordertag">
								<h4>Order Details</h4>
							</div>
							<div class="col-md-12">
								<div>
									<div class="col-md-7">
										<h5>Nama product</h5>
									</div>
									<div class="col-md-1">
										<h5>Qty</h5>
									</div>
									<div class="col-md-4">
										<h5>Subtotal</h5>
									</div>
								</div>
							</div>
							<div class="order col-md-12">
								<!-- list order -->
							</div>
							<div class="col-md-12" style="background:black"></div>
							<div class="ship col-md-12">
								<h5 class="col-md-8">Shipping fee</h5>
								<h5 class="shipfee col-md-4">20.000</h5>
							</div>
							<div class="ship col-md-12">
								<h5 class="col-md-8">Total</h5>
								<h5 class="total col-md-4">120000</h5>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	<div class="masterorder" style="display:none">
		<div class="name col-md-7">
			<h5>Nama product</h5>
		</div>
		<div class="qty col-md-1">
			<h5>2</h5>
		</div>
		<div class="subtotal col-md-4">
			<h5>20000</h5>
		</div>
	</div>
@endsection