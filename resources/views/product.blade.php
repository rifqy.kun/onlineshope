@extends('template.template')

@section('title'.'Product')

@section('content')
<!-- Breadcrumb -->
<div class="page-info-section page-info">
		<div class="container">
			<div class="site-breadcrumb">
				<a href="/">Home</a> /
				<span>Product</span>
			</div>
			<img src="{{asset('img/page-info-art.png')}}" alt="" class="page-info-art">
		</div>
	</div>
<!-- Breadcrumb end -->
<section class="product-section spad">
		<div class="container">
			<ul class="product-filter controls">
				<li class="control" data-filter=".new">New arrivals</li>
				<li class="control" data-filter="all">Recommended</li>
				<li class="control" data-filter=".best">Best sellers</li>
			</ul>
			<div class="row" id="product-filter">
			@foreach($product as $p)
				<div class="col-lg-3 col-md-6 best">
					<div class="product-item">
						<figure>
							<img src="{{asset('img/products/1.jpg')}}" alt="">
							<div class="pi-meta">
								<div class="pi-m-left">
									<img src="{{asset('img/icons/eye.png')}}" alt="">
									<p>quick view</p>
								</div>
								<div class="pi-m-right">
									<img src="{{asset('img/icons/heart.png')}}" alt="">
									<p>save</p>
								</div>
							</div>
						</figure>
						<div class="product-info">
							<input type="hidden" name="product_id" value="{{$p->id}}">
							<h6>{{$p->name}}</h6>
							<p>Rp. {{$p->price}}</p>
							<a style="cursor:pointer;" data-idproduct ="{{$p->id}}" data-name="{{$p->name}}" data-harga="{{$p->price}}" class="btncart btn-line">
								ADD TO CART
							</a>
							<br>
							<br>
							<a style="cursor:pointer;" href="{{url('product/')}}/{{$p->id}}" class="btndetail btn-line">Details</a>
						</div>
					</div>
				</div>
			@endforeach
			</div>
			{{$product->links()}}
		</div>
	</section>
	@endsection