@extends('admin_v.template.templateadmin')
@section('title','Category')

@section('contentadmin')
<section class="content-header">
    <h1>
        Category
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="fa fa-dashboard"></i>
                Home</a>
        </li>
        <li class="active">Category</li>
    </ol>
</section>
@if( Session::has("success"))
<div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <i class="fa fa-check-circle"></i>
    {{Session::get('success')}}
</div>
@endif
<div class="container">
<br/>
<a href="/dashboard/category/add" class="btn btn-primary">Tambah Category</a>
<br>
<br>
<table class="table table-striped">
    <thead>
        <tr>
            <th>No</th>
            <th>ID Category</th>
            <th>Name</th>
            <th>Urutan</th>
            <th colspan="2">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($category as $c)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$c->id}}</td>
            <td>{{$c->categorie}}</td>
            <td>{{$c->urutan}}</td>
            <td>
            <a href="{{url('dashboard/category/edit/')}}/{{$c->id}}" class="btn btn-info">Edit</a>
            <a href="{{url('dashboard/category/delete/')}}/{{$c->id}}" class="btn btn-danger" onclick="return confirm('Are you sure ?')">Delete</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$category->links()}}
</div>
@endsection