@extends('admin_v.template.templateadmin')
@section('title','Edit Category')

@section('contentadmin')
<section class="content-header">
    <h1>
        Category
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="fa fa-dashboard"></i>
                Home</a>
        </li>
        <li class="active">Edit Category</li>
    </ol>
    </section>
<div class="main">
	<div class="main-content">
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<!-- INPUTS -->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Edit Category</h3>
						</div>
						<div class="panel-body">
							<form action="{{$category->id}}" method="POST">
								@csrf 
								<input type="text" class="form-control @error('category') invalid is-invalid @enderror" name="category" placeholder="Category" value="@if(old('category')) {{old('category')}} @else {{$category->categorie}} @endif">
								@error('category')
								<span class="invalid"><i>{{$message}}</i></span>
								@enderror
								</br>
								<button type="submit" class="btn btn-primary" style="margin-top: 20px;">Edit Category</button>
							</form>
						</div>
					</div>
					<!-- END INPUTS -->
				</div>
			</div>
		</div>
	</div>
</div>
<style>
.invalid{
	display:block;
	color:red;
}
.is-invalid{
	color:grey;
	border:1px solid red;
}
</style>
@endsection