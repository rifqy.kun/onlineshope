@extends('admin_v.template.templateadmin')
@section('title','Product')

@section('contentadmin')
<section class="content-header">
    <h1>
        Product
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="fa fa-dashboard"></i>
                Home</a>
        </li>
        <li class="active">Edit Product</li>
    </ol>
</section>
<div class="main">
	<div class="main-content">
		<div class="container-fluid panel">
			<div class="row panel-heading">
				<div class="col-md-3">
					<div>
						<h3 class="panel-title">Edit Product</h3>
					</div>
				</div>
			</div>
			<div class="row panel-body">
				<div class="">
					<form action="{{$product->id}}" method="POST">
						@csrf
						<div class="form-group">
                            <label for="">Nama Produk</label>
							<input type="text" name="name" class="form-control" placeholder="Name" value="@if(old('name')) {{old('name')}} @else {{$product->name}} @endif">
                            @error('name')
								<span class="invalid"><i>{{$message}}</i></span>
							@enderror
						</div>
						<div class="form-group">
                            <label for="">Harga Produk</label>
							<input type="text" name="price" class="form-control" placeholder="Price" value="@if(old('price')) {{old('price')}} @else {{$product->price}} @endif">
                            @error('price')
								<span class="invalid"><i>{{$message}}</i></span>
							@enderror
                        </div>
						<div class="form-group">
                            <label for="">Stok Produk</label>
							<input type="text" name="stock" class="form-control" placeholder="Stock" value="@if(old('stock')) {{old('stock')}} @else {{$product->stock}} @endif">
                            @error('stock')
								<span class="invalid"><i>{{$message}}</i></span>
							@enderror
                        </div>
						<div class="form-group">
                            <label for="">Deskripsi Produk</label>
							<input type="text" name="description" class="form-control" placeholder="Description" value="@if(old('description')) {{old('description')}} @else {{$product->description}} @endif">
                            @error('description')
								<span class="invalid"><i>{{$message}}</i></span>
							@enderror
                        </div>
                        <div class="form-group">
                            <label for="">Categori Produk</label>
                        <select name="category" class="form-control">
                            @foreach($select_category as $s)
                                <option value="{{$s->id}}" @if( $s->id == old("category",$product->category_id)) selected @endif>
                                    {{$s->categorie}}
                                </option>
                            @endforeach
                         </select>
                         @error('category')
								<span class="invalid"><i>{{$message}}</i></span>
						@enderror
						</div>
						<button type="submit" class="btn btn-primary mt-5">Edit New Product</button>
					</form>
				</div>
			</div>		
		</div>
	</div>
</div>
@endsection