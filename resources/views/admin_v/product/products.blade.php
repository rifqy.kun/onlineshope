@extends('admin_v.template.templateadmin')
@section('title','Product')

@section('contentadmin')
<section class="content-header">
    <h1>
        Product
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="fa fa-dashboard"></i>
                Home</a>
        </li>
        <li class="active">Product</li>
    </ol>
</section>
<!-- @if( Session::has("success"))
<div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <i class="fa fa-check-circle"></i>
    {{Session::get('success')}}
</div>
@endif -->
</br>
<div class="container">
<br/>
<a href="/dashboard/product/add" class="btn btn-primary">Tambah Product</a>
<br>
<br>
<table class="table table-striped">
    <thead>
        <tr>
            <th>ID</th>
            <th>Nama Product</th>
            <th>Price</th>
            <th>Stock</th>
            <th>Description</th>
            <th>Category</th>
            <th colspan="2">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($product as $p)
        <tr>
            <td>{{$p->id}}</td>
            <td>{{$p->name}}</td>
            <td>{{$p->price}}</td>
            <td>{{$p->stock}}</td>
            <td>{{$p->description}}</td>
            <td>{{$p->categorie}}</td>
            <td>
            <a href="{{url('dashboard/product/edit/')}}/{{$p->id}}" class="btn btn-info">Edit</a>
            <a href="{{url('dashboard/product/delete/')}}/{{$p->id}}" class="btn btn-danger" onclick="return confirm('Are you sure ?')">Delete</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$product->links()}}
</div>
@endsection