@extends('admin_v.template.templateadmin')
@section('title','Product')

@section('contentadmin')
<section class="content-header">
    <h1>
        Product
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="fa fa-dashboard"></i>
                Home</a>
        </li>
        <li class="active">Tambah Product</li>
    </ol>
</section>
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="">
                    <div class="panel">
                        <div class="panel-heading">
                            <div>
                                <h3 class="panel-title">New product</h3>
                            </div>
                        </div>
                        <div class="panel-body">
                        <form action="" method="POST" autocomplete="off" id="form-input">
								@csrf
								<div class="form-group">
                                <label name="name">Nama produk</label>
									<input type="text" name="name" class="form-control @error('name') is-invalid invalid @enderror" placeholder="Name" value="{{old('name')}}">
									@error('name')
									 <span class="invalid"><i>{{$message}}</i></span>
									@enderror
								</div>							
								<div class="form-group">
                                <label name="price">Harga produk</label>
									<input type="text" name="price" class="form-control @error('price') is-invalid invalid @enderror" placeholder="Price" value="{{old('price')}}">
									@error('price')
									 <span class="invalid"><i>{{$message}}</i></span>
									@enderror
								</div>			
								<div class="form-group">
                                <label name="stock">Stok produk</label>
									<input type="text" name="stock" class="form-control @error('stock') is-invalid invalid @enderror" placeholder="Stock" value="{{old('stock')}}">
									@error('stock')
									 <span class="invalid"><i>{{$message}}</i></span>
									@enderror
                                </div>
                                <div class="form-group">
                                <label name="description">Deskripsi produk</label>
                                    <textarea name="description" class="form-control @error('description') is-invalid invalid @enderror" placeholder="Description" value="{{old('description')}}">{{old('description')}}</textarea>
                                    @error('description')
									 <span class="invalid"><i>{{$message}}</i></span>
									@enderror
                                </div>
                                <div class="form-group">
                                <label name="category">Categori produk</label>
									<select name="category_id" class="form-control @error('category_id') is-invalid invalid @enderror" placeholder="Category">
                                    <option value="">Pilih Category</option>
                                        @foreach($select_category as $s)
                                        <option value="{{$s->id}}">{{$s->categorie}}</option>
                                        @endforeach
                                    </select>
									@error('category_id')
									 <span class="invalid"><i>{{$message}}</i></span>
									@enderror
								</div>
								<div class="form-group">
									<label for="picture">Picture</label>
								    <input type="file" name="picture" class="form-control-file @error('picture') is-invalid invalid @enderror" id="picture" value="{{old('picture')}}">
								    @error('picture')
									 <span class="invalid"><i>{{$message}}</i></span>
									@enderror
								</div>
								<button type="submit" class="btn btn-primary" style="margin-top: 10px;">Add New Product</button>
							</form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection