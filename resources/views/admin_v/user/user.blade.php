@extends('admin_v.template.templateadmin')
@section('title','User')

@section('contentadmin')
<section class="content-header">
      <h1>
        Data User
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">User</li>
      </ol>
</section>
<br>
<section>
  <div class="container">
    <table class="table table-striped">
      <thead>
        <tr>
          <th>No</th>
          <th>Nama</th>
          <th>Email</th>
          <th>Level</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        @foreach($user as $u)
        <tr>
          <td>{{$loop->iteration}}</td>
          <td>{{$u->name}}</td>
          <td>{{$u->email}}</td>
          <td>{{$u->level}}</td>
          <td>
            <a class="btn btn-warning">Edit</a>
            <a class="btn btn-danger">Delete</a>
          </td>
        </tr>
        @endforeach
      </tbody>
      <tbody></tbody>
    </table>
  </div>
</section>
@endsection