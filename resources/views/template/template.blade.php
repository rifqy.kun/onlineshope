<!DOCTYPE html>
<html lang="zxx">
    <head>
        <title>@yield('title')</title>
        <meta charset="UTF-8">
        <meta name="description" content="The Plaza eCommerce Template">
        <meta name="keywords" content="plaza, eCommerce, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="csrf-token" content="{!! csrf_token() !!}">
        <!-- Favicon -->
        <link href="img/favicon.ico" rel="shortcut icon"/>

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <!-- Stylesheets -->
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}"/>
        <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}"/>
        <link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}"/>
        <link rel="stylesheet" href="{{asset('css/style.css')}}"/>
        <link rel="stylesheet" href="{{asset('css/sidebar.css')}}"/>
        <link rel="stylesheet" href="{{asset('css/animate.css')}}"/>
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <!-- sidebar stylesheets -->
        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css"> -->
        <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
        <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

        
    </head>
    <body>
    <div class="wrapper">
        <!-- Page Preloder -->
        <div id="preloder">
            <div class="loader"></div>
        </div>
            <!-- sidebar -->
        <nav id="sidebar">
            <div id="dismiss">
                <i class="fa fa-arrow-left"></i>
            </div>
            <div id="refresh">
                <i class="glyphicon glyphicon-refresh"></i>
            </div>
            <div class="sidebar-header">
                <h3 style="text-align:center">Keranjang</h3>
            </div>
            <div class="loading">
                <p style="color:white">loading. . . . .</p>
            </div>
            <div class="isitemplate">
                
            </div>
            <div class="checkout">
                <div class="item">
                    <a class="btn btn-danger" href="/order">Checkout</a>
                    <a class="btn btn-warning">Shopping</a>
                </div>
            </div>
            
        </nav>
            <!-- endsidebar -->
        <!-- Header section -->
        <header class="header-section header-normal">
            <div class="container-fluid">
                <!-- logo -->
                <div class="btn-sidebar">
                    <button type="button" id="sidebarCollapse" class="btn btn-light">
                        <i class="fa fa-align-left"></i>
                    </button>
                </div>
                <div class="site-logo">
                    <img src="{{asset('img/logo.png')}}" alt="logo"></a>
                </div>
                <!-- responsive -->
                <div class="nav-switch">
                    <i class="fa fa-bars"></i>
                </div>
                <div class="header-right">
                    <a href="#" class="card-bag">
                        <img src="{{asset('img/icons/bag.png')}}" alt="">
                        <span>2</span>
                    </a>
                    <a href="#" class="search">
                        <img src="{{asset('img/icons/search.png')}}" alt=""></a>
                </div>
                <!-- site menu -->
                <ul class="main-menu">
                    <li>
                        <a href="/">Home</a>
                    </li>
                    <li>
                        <a href="/product">Shop Now</a>
                    </li>
                    <li>
                        <a href="/contact">Contact</a>
                    </li>
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a style="color:black" class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                </ul>
            </div>
        </header>
        <!-- Header section end -->

        <!-- Product section -->
        @yield('content')
        <br/>
        <br/>
        <!-- Product section end -->

        <!-- Footer top section -->
        <section class="footer-top-section home-footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-8 col-sm-12">
                        <div class="footer-widget about-widget">
                            <img src="img/logo.png" class="footer-logo" alt="">
                            <p>Donec vitae purus nunc. Morbi faucibus erat sit amet congue mattis. Nullam
                                fringilla faucibus urna, id dapibus erat iaculis ut. Integer ac sem.</p>
                            <div class="cards">
                                <img src="{{asset('img/cards/5.png')}}" alt="">
                                <img src="{{asset('img/cards/4.png')}}" alt="">
                                <img src="{{asset('img/cards/3.png')}}" alt="">
                                <img src="{{asset('img/cards/2.png')}}" alt="">
                                <img src="{{asset('img/cards/1.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-6">
                        <div class="footer-widget">
                            <h6 class="fw-title">usefull Links</h6>
                            <ul>
                                <li>
                                    <a href="#">Partners</a>
                                </li>
                                <li>
                                    <a href="#">Bloggers</a>
                                </li>
                                <li>
                                    <a href="#">Support</a>
                                </li>
                                <li>
                                    <a href="#">Terms of Use</a>
                                </li>
                                <li>
                                    <a href="#">Press</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-6">
                        <div class="footer-widget">
                            <h6 class="fw-title">Sitemap</h6>
                            <ul>
                                <li>
                                    <a href="#">Partners</a>
                                </li>
                                <li>
                                    <a href="#">Bloggers</a>
                                </li>
                                <li>
                                    <a href="#">Support</a>
                                </li>
                                <li>
                                    <a href="#">Terms of Use</a>
                                </li>
                                <li>
                                    <a href="#">Press</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-6">
                        <div class="footer-widget">
                            <h6 class="fw-title">Shipping & returns</h6>
                            <ul>
                                <li>
                                    <a href="#">About Us</a>
                                </li>
                                <li>
                                    <a href="#">Track Orders</a>
                                </li>
                                <li>
                                    <a href="#">Returns</a>
                                </li>
                                <li>
                                    <a href="#">Jobs</a>
                                </li>
                                <li>
                                    <a href="#">Shipping</a>
                                </li>
                                <li>
                                    <a href="#">Blog</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-6">
                        <div class="footer-widget">
                            <h6 class="fw-title">Contact</h6>
                            <div class="text-box">
                                <p>Your Company Ltd
                                </p>
                                <p>1481 Creekside Lane Avila Beach, CA 93424,
                                </p>
                                <p>+53 345 7953 32453</p>
                                <p>office@youremail.com</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Footer top section end -->

        <!-- Footer section -->
        <footer class="footer-section">
            <div class="container">
                <p class="copyright">
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY
                    3.0. -->
                    Copyright &copy;<script>
                        document.write(new Date().getFullYear());
                    </script>
                    All rights reserved | This template is made with
                    <i class="fa fa-heart-o" aria-hidden="true"></i>
                    by
                    <a href="https://colorlib.com" target="_blank">Colorlib</a>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY
                    3.0. -->
                </p>
            </div>
        </footer>
    </div>

    <div class="overlay"></div>
            <!-- template sidebar -->
            <div class="mastersidebar" style="display:none">
                <div class="gambar">
                    <img src="{{asset('img/products/1.jpg')}}" class="pict" width="80px" style="margin-left:75px">
                </div>
                <div class="nama">
                    <h5>Nama produk</h5>
                </div>
                <div class="harga">
                    <h5>Harga Produk</h5>
                </div>
                <div id="field1">Qty
                    <button type="button" id="" class="sub">-</button>
                    <input type="number" class="qty" id="number" value="1" min="1" max="999"/>
                    <button type="button" id="" class="add">+</button>
                    <span><button class="btn btn-danger delete">delete</button></span>
                </div>
                </br>
            </div>
        <!-- endtemplate -->

        <!-- templatecart -->
        <!-- <div class="cartover">
            <div>
                <p>keranjang</p>
            </div>
        </div> -->
        <!-- endtemplatecart -->
        <!-- Footer section end -->

        <!--====== Javascripts & Jquery ======-->
        <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <script src="{{asset('js/owl.carousel.min.js')}}"></script>
        <script src="{{asset('js/mixitup.min.js')}}"></script>
        <script src="{{asset('js/sly.min.js')}}"></script>
        <script src="{{asset('js/jquery.nicescroll.min.js')}}"></script>
        <script src="{{asset('js/main.js')}}"></script>
        <script src="{{asset('js/sidebar.js')}}"></script>
        <script src="{{asset('js/app.js')}}" defer></script>

        <!-- sidebar jquery -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
        <script>
        //    $(document).ready(function(){
        //     //    $(".btncart").click(function(){

        //     //         var name = $(this).data("name");
        //     //         var price = $(this).data("harga");
        //     //         let gambar = $(this).parent().parent().find("img").attr("src");
                    
        //     //         var $ob = $(".mastersidebar:first").clone(true);

        //     //         $ob.show();
        //     //         $ob.find(".nama").html(name);
        //     //         $ob.find(".harga").html("Rp. "+price);
        //     //         $ob.find(".pict").attr("src",gambar);

        //     //         $ob.find("#delete").click(function()
        //     //         {
                        
        //     //             $(this).parent().parent().parent().remove();
                        
        //     //         });

        //     //        $("#sidebar").addClass('active').append($ob);
        //     //    });

              
        //    });
        // </script>
     </body>
</html>