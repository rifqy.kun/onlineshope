$(document).ready(function () {

    
    $('#dismiss, .overlay').on('click', function () {
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    });

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').addClass('active');
        $('.overlay').addClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });

    $("select.shipping").change(function(){
        var selectedshipping = $(".shipping option:selected").text();
        if (selectedshipping == "JNE"){
            $(".shippingfee").attr("value",20000);
        }else if (selectedshipping == "JNT"){
            $(".shippingfee").attr("value",22000);
        }else if (selectedshipping == "SICEPAT"){
            $(".shippingfee").attr("value",19000);
        }else if (selectedshipping == "POS INDONESIA"){
            $(".shippingfee").attr("value",21000);
        }else if (selectedshipping == "Select Shipping Service *"){
            $(".shippingfee").attr("value","Please select Shipping Service (jasa kirim)");
        }
    });

    // $(".header-right .card-bag").mouseover(function(){
    //     $(".cartover").slideDown();
    // })
    // .mouseout(function(){
    //     $(".cartover").slideUp();
    // })
});