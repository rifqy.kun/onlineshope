/* =================================
------------------------------------
	The Plaza - eCommerce Template
	Version: 1.0
 ------------------------------------ 
 ====================================*/


'use strict';


$(window).on('load', function() {
	/*------------------
		Preloder
	--------------------*/
	$(".loader").fadeOut(); 
	$("#preloder").delay(400).fadeOut("slow");


	/*------------------
		Product filter
	--------------------*/
	if($('#product-filter').length > 0 ) {
		var containerEl = document.querySelector('#product-filter');
		var mixer = mixitup(containerEl);
	}

});

(function($) {
	/*------------------
		Navigation
	--------------------*/
	$('.nav-switch').on('click', function(event) {
		$('.main-menu').slideToggle(400);
		event.preventDefault();
	});


	/*------------------
		Background Set
	--------------------*/
	$('.set-bg').each(function() {
		var bg = $(this).data('setbg');
		$(this).css('background-image', 'url(' + bg + ')');
	});


	/*------------------
		Hero Slider
	--------------------*/
	$('.hero-slider').owlCarousel({
		loop: true,
		nav: true,
		navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
		dots: true,
		mouseDrag: false,
		animateOut: 'fadeOut',
		animateIn: 'fadeIn',
		items: 1,
		autoplay: true
	});


	/*------------------
		Intro Slider
	--------------------*/
	if($('.intro-slider').length > 0 ) {
		var $scrollbar = $('.scrollbar');
		var $frame = $('.intro-slider');
		var sly = new Sly($frame, {
			horizontal: 1,
			itemNav: 'forceCentered',
			activateMiddle: 1,
			smart: 1,
			activateOn: 'click',
			//mouseDragging: 1,
			touchDragging: 1,
			releaseSwing: 1,
			startAt: 10,
			scrollBar: $scrollbar,
			//scrollBy: 1,
			activatePageOn: 'click',
			speed: 200,
			moveBy: 600,
			elasticBounds: 1,
			dragHandle: 1,
			dynamicHandle: 1,
			clickBar: 1,
		}).init();
	}



	/*------------------
		ScrollBar
	--------------------*/
	$(".cart-table, .product-thumbs").niceScroll({
		cursorborder:"",
		cursorcolor:"#afafaf",
		boxzoom:false
	});



	/*------------------
		Single Product
	--------------------*/
	$('.product-thumbs-track > .pt').on('click', function(){
		var imgurl = $(this).data('imgbigurl');
		var bigImg = $('.product-big-img').attr('src');
		if(imgurl != bigImg) {
			$('.product-big-img').attr({src: imgurl});
		}
	})

})(jQuery);

$(document).ready(function()
{
	refreshsidebar();
	function refreshsidebar()	
	{
		$("#sidebar .loading").show();
		$(".isitemplate").html("");
		$.get( "http://localhost:8000/cart/show", function( data )
		{
			$("#sidebar .loading").hide();
			// console.log(data.list);
			data.list.forEach(myFunction);

			function myFunction(item, index)
			{
				let cart = $(".mastersidebar:first").clone();
				cart.find(".nama").html("Nama produk : "+item.nama);
				cart.find(".harga").html("Harga : Rp. "+item.harga);
				cart.find(".qty").attr("value",item.qty);
				cart.show();

				cart.find(".delete").click(function(){
					
					$.get( "http://localhost:8000/cart/delete/"+item.id, function( jasonhtml ){
						refreshsidebar();

					});
				});
				$(".isitemplate").append(cart);
				cart.find(".add").click(function(){
					let id = item.id;
					let add = $(this).attr("class")

					$.post("http://localhost:8000/cart/qty/id",{"_token": $('meta[name="csrf-token"]').attr('content') ,"id":id, "add":add})
					refreshsidebar();
				});
				cart.find(".sub").click(function(){
					let id = item.id;
					let min = $(this).attr("class")

					$.post("http://localhost:8000/cart/qty/id",{"_token": $('meta[name="csrf-token"]').attr('content') ,"id":id, "min":min})
					refreshsidebar();
				});
			}

			var nama = " dodi " ; 
			var nama_lengkap = "nama saya "+nama;
		});
	}
	$("#refresh").click(function()
	{
		refreshsidebar();
	})

	$(".btncart").click(function()
	{
		let product_id = $(this).data("idproduct");
		let quantity;
		
		$.post( "http://localhost:8000/cart/show",{"_token": $('meta[name="csrf-token"]').attr('content') ,"product":product_id, quantity}, function( jasonhtml )
		{
			refreshsidebar() ; 
			$('#sidebar').addClass('active');
		});
		
	});

})