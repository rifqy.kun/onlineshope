<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    public function product()
    {
        $product = Product::paginate(8);
        return view('product', compact('product'));
    }
    public function detail($id)
    {
        $product = Product::find($id);
        return view('pdetail', compact('product'));
    }
}
