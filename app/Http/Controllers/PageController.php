<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function contact()
    {
        $title = "Halaman Contact Web";
        return view('contact', compact('title'));
    }
    public function aboutus()
    {
        $title = "Halaman About Us Web";
        return view('aboutus', compact('title'));
    }
    public function blog()
    {
        return view('blog');
    }
}
