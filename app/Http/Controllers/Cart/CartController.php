<?php

namespace App\Http\Controllers\Cart;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Product;
use App\Cart;

class CartController extends Controller
{
    public function cart()
    {
        
        if(Auth::id()==null){
            $gagal=["pesan"=>"Anda belum login", "status"=>0];

            return response()->json($gagal);
        }
        $cart = Cart::where('user_id', Auth::id())->orderBy("id","DESC")->get(); 
        // $query = "SELECT * FROM cart WHERE user_Id = ".Auth::id()." ORDER BY id DESC" ;
        // DB::select("*")->from("cart")->where()->orderBy()->get();
        $array = array();
        $status = array(); 
        foreach ($cart as $c)
        {   
            
            $list[] = ["id"=>$c->id, 
                        "nama"=>$c->product->name, 
                        "harga"=>$c->product->price, 
                        "qty"=>$c->qty];
        }
        
        $status         = ["pesan"=>"Berhasil","status"=>1,"user_id"=>Auth::id()];
        $status['list'] = $list;
        
        // $status['coba'] = ["pro1"=>"Baju","pro2"=>"Celana","detail"=>["x","L","M","varian"=>["merah","kuning","hijau"]]];
        return response()->json($status);
    }
    public function cartpost(Request $request)
    {

            $find = Cart::where([
                ['user_id', Auth::id()],
                ['product_id', $request->product],
            ])->count();

            $cart = Cart::where([
                ['user_id', Auth::id()],
                ['product_id', $request->product],
            ])->first();
   
            if($find >= 1)
            {
                
                $quantity = $cart->qty;
                $cart->qty = $quantity + 1;
                $cart->save();
            }else
            {
               
                $newcart  = new Cart;
                $newcart->user_id = Auth::id();
                $newcart->product_id = $request->product;
                $newcart->qty = 1;
                $newcart->save();
            }
    }
    public function destroy($id)
    {
        $cart = Cart::find($id);
        Cart::find($id)->delete();
    }
    public function quantity(Request $request)
    {
        $add = ($request->add);
        $min = ($request->min);
        $cart = Cart::find($request->id);

        if($add == "add")
        {
            $cart->qty = $cart->qty + 1;
            $cart->save();
        }elseif($min == "sub")
        {
            if($cart->qty > 1)
            {
                $cart->qty = $cart->qty -1;
                $cart->save();
            }elseif($cart->qty == 1)
            {
                   $cart->delete();
            }
        }
    

    }
}
