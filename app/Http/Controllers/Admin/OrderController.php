<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public function order()
    {
        $order = DB::table('orders')
                    ->join('users', 'orders.user_id','=', 'users.id')
                    ->select('orders.*', 'users.name', 'users.email')
                    ->get();
                    dd($order);
        return view('admin_v.order.order');
    }
}
