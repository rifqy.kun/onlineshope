<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Product;
use App\Category;

class ProductController extends Controller
{
    public function product()
    {
        $product = DB::table('products')
                    ->join('categories', 'categories.id','=','products.category_id')
                    ->select('products.*', 'categories.categorie')
                    ->paginate(5);
        return view('admin_v.product.products', compact('product'));
    }
    public function addproduct()
    {
        $select_category = Category::all();
        return view('admin_v.product.addproduct', compact('select_category'));
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'     =>'required|string',
            'price'    =>'required|string',
            'stock'    =>'required|integer',
            'description' => 'required|string',
            'category_id' => 'required|integer'
        ]);

        $product = new Product;
        $product->name = $request->name;
        $product->price = $request->price;
        $product->stock = $request->stock;
        $product->description = $request->description;
        $product->category_id = $request->category_id;

        $product->save();

        return redirect('dashboard/product');

    }
    public function destroy($id)
    {
        $product = Product::find($id);
        Product::find($id)->delete();

        return redirect('/dashboard/product');
    }
    public function updateproduct($id)
    {
        $select_category = Category::all();
        $product = Product::find($id);
        return view('admin_v.product.editproduct', compact('product','select_category'));
    }
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'     =>'required|string',
            'price'    =>'required|string',
            'stock'    =>'required|integer',
            'description' => 'required|string',
            'category' => 'required|integer'
        ]);

        // $category = Category::find($id);
        $product = Product::find($id);
        $product->name = $request->name;
        $product->price = $request->price;
        $product->stock = $request->stock;
        $product->description = $request->description;
        $product->category_id = $request->category;
        $product->save();

        return redirect('/dashboard/product');
    }
}
