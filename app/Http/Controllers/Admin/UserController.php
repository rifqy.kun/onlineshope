<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function user()
    {
        $user = User::all();
        return view('admin_v.user.user', compact('user'));
    }
}
