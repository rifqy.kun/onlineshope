<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
Use App\Category;
use Session;

class CategoryController extends Controller
{
    public function category()
    {
        $category = Category::paginate(5);
        return view('admin_v.category.category', compact('category'));
    }
    public function addcategory()
    {
        return view('admin_v.category.addcategory');
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'category'=>'required|string|max:20'
        ]);

        $urutan = Category::max('urutan')+1;
        $category = new Category;
        $category->categorie = $request->category;
        $category->urutan = $urutan;
        $category->save();

        Session::flash('success', 'Data berhasil ditambahkan!');

        return redirect('dashboard/category');
    }
    public function destroy($id)
    {
        $category = Category::find($id);
        Category::find($id)->delete();

        Session::flash('success', 'Data berhasil dihapus!');

        return redirect('/dashboard/category');
    }
    public function updatecategory($id)
    {
        $category = Category::find($id);
        return view('admin_v.category.editcategory', compact('category'));
    }
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'category'=>'required|string|max:20'
        ]);

        $category = Category::find($id);
        $category->categorie = $request->category;
        $category->save();

        Session::flash("success","Data has been succesfully update");

        return redirect('dashboard/category');
    }
}
