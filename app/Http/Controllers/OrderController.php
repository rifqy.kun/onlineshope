<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Product;
use App\Cart;
use App\Order;
use App\Orderdetail;

class OrderController extends Controller
{
    public function order()
    {
        return view('order');
    }
    public function ordershow()
    {
        $cart = Cart::where('user_id', Auth::id())->orderBy("id","Desc")->get();
        $array = array();
        $status = array();
        foreach ($cart as $c)
        {
            $list[] = [
                "id"=>$c->id,
                "nama"=>$c->product->name,
                "harga"=>$c->product->price,
                "qty"=>$c->qty
            ];
        }
        $status = ["pesan"=>"Berhasil","status"=>1,"user_id"=>Auth::id()];
        $status['list'] = $list;

        return response()->json($status);
    }
    public function purchase(Request $request)
    {
        
    }
}
