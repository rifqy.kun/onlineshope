<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                'name' => 'Anto',
                'email' => 'anto@admin.com',
                'password' => bcrypt('12345678'),
                'level' => 'admin'
            ]
        );
    }
}
