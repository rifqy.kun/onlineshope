<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Categories')->insert([
            'id' => '1',
            'categorie' => 'celana',
            'urutan' =>0
            
        ]);
        
        DB::table('Products')->insert([
            'category_id' => '1',
            'name' => 'Celana Jean',
            'price' => '100000',
            'stock' => '6',
            'description' => 'Celana murah',
        ]);

       
    }
}
