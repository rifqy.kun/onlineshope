<?php

use Illuminate\Database\Seeder;

class CartSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('carts')->insert(
            [
                'product_id' => '1',
                'user_id' => '1',
                'qty' => '4'
            ]
        );
    }
}
