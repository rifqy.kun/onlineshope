<?php
use App\Http\Middleware\CheckAdmin;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'HomeController@index');
Route::get('/contact', 'PageController@contact');
Route::get('/aboutus', 'PageController@aboutus');
Route::get('/blog', 'PageController@blog');
Route::get('/product', 'ProductController@product');
Route::get('/categorie', 'ProductController@categorie');
Route::get('/product/{id}', 'ProductController@detail');
Route::get('/cart', 'CartController@cart');
Route::get('/checkout', 'ChartController@checkout');
Route::get('/order', 'OrderController@order');


Route::prefix('dashboard')->middleware(CheckAdmin::class)->group(function()
{
    Route::get('/', 'Admin\PageController@dashboard');
    
    Route::get('/user', 'Admin\UserController@user');

    // Category Route
    Route::get('/category', 'Admin\CategoryController@category');
    Route::get('/category/add', 'Admin\CategoryController@addcategory');
    Route::post('/category/add', 'Admin\CategoryController@store');
    Route::get('/category/delete/{id}', 'Admin\CategoryController@destroy');
    Route::get('/category/edit/{id}', 'Admin\CategoryController@updatecategory');
    Route::post('/category/edit/{id}', 'Admin\CategoryController@update');

    // Product Route
    Route::get('/product', 'Admin\ProductController@product');
    Route::get('/product/add', 'Admin\ProductController@addproduct');
    Route::post('/product/add', 'Admin\ProductController@store');
    Route::get('/product/delete/{id}', 'Admin\ProductController@destroy');
    Route::get('/product/edit/{id}', 'Admin\ProductController@updateproduct');
    Route::post('/product/edit/{id}', 'Admin\ProductController@update');

    Route::get('/order', 'Admin\OrderController@order');
});

// Cart Route
Route::get('/cart/show', 'Cart\CartController@cart');
Route::post('/cart/show', 'Cart\CartController@cartpost');
Route::get('/cart/delete/{id}', 'Cart\CartController@destroy');
Route::post('/cart/qty/{id}/', 'Cart\CartController@quantity');

// Order Route
Route::get('/order', 'OrderController@order');
Route::get('/order/show', 'OrderController@ordershow');
Route::get('/order/purchase', 'OrderController@purchase');